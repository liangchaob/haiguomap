# 海国Map

#### 介绍
一个用于汇总全球国家信息网站, 用于快速查询一个任意国家基本信息与情况, 地理与旅游党必备手册.

#### 软件架构
纯静态数据站

#### 网址
https://map.liangchao.com

#### 截图
![](http://ipic.liangchao.site/2020-06-15-FireShot%20Capture%20292%20-%20%E6%B5%B7%E5%9B%BDData%20-%20https___data.liangchao.site_.jpg)

![](http://ipic.liangchao.site/2020-06-15-FireShot%20Capture%20294%20-%20%E6%B5%B7%E5%9B%BDData%20-%20https___data.liangchao.site_-.jpg)

![](http://ipic.liangchao.site/2020-06-15-FireShot%20Capture%20293%20-%20%E6%B5%B7%E5%9B%BDData%20-%20https___data.liangchao.site_-.jpg)
