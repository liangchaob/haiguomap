#!/usr/bin/python
# encoding:utf-8
from download_data import download_countries, download_flags
from clean_data import clean_all_data

# 接口信息
apiurl = "https://api.jisuapi.com/country/detail"
appkey = ""

# 数据文件夹
origindir = "../data/data_origin/"
cleaneddir = "../data/data_cleaned/"
resultdir = "../data/data_result/"
flagsdir = "../data/data_flags/"
flagneturl = "flags/"

def main():
    # # 下载 json 数据
    download_countries(apiurl, appkey, origindir)
    # # 清洗数据
    clean_all_data(origindir, cleaneddir, resultdir)
    # 下载清洗后的全部国家国旗
    download_flags(resultdir, flagsdir, flagneturl)

if __name__ == '__main__':
    main()